
# EXAMPLE INPUT DATAPOINT
#{"id":1,
#"org_id":"83e74666-0836-4c1d-948a-4b34a8b90301",
#"dept_id":"860a9509-0c3c-4a31-b116-cd8bbff0afce",
#"provider_type":"SELF_PRODUCED",
#"data_source_url":"www.hel.fi",
#"name_fi":"Päiväkoti Apila",
#"name_sv":"Päiväkoti Apila",
#"name_en":"Day care Apila",
#"ontologyword_ids":[160, 288, 663],
#"ontologytree_ids":[870, 871, 879, 978, 1090, 2125],
#"latitude":60.2444,
#"longitude":24.855799,
#"northing_etrs_gk25":6681311,
#"easting_etrs_gk25":25492013,
#"northing_etrs_tm35fin":6680558,
#"easting_etrs_tm35fin":381299,
#"manual_coordinates":true,
#"street_address_fi":"Arentikuja 3",
#"street_address_sv":"Arrendegränden 3",
#"street_address_en":"Arentikuja 3",
#"address_zip":"00410",
#"address_city_fi":"Helsinki",
#"address_city_sv":"Helsingfors",
#"address_city_en":"Helsinki",
#"phone":"09 310 41571, 09 310 46996",
#"call_charge_info_fi":"pvm/mpm",
#"call_charge_info_sv":"lna/msa",
#"call_charge_info_en":"local network charge/mobile call charge",
#"fax":"09 310 41571",
#"email":"pk.apila@hel.fi",
#"www_fi":"https://www.hel.fi/varhaiskasvatus/fi/paivakodit/apila",
#"www_sv":"https://www.hel.fi/varhaiskasvatus/fi/paivakodit/apila",
#"www_en":"https://www.hel.fi/varhaiskasvatus/fi/paivakodit/apila",
#"address_postal_full_fi":"PL 41910, 00099 Helsingin kaupunki",
#"address_postal_full_sv":"PB 41910, 00099 Helsingfors stad",
#"address_postal_full_en":"P.O. Box 41910, 00099 City of Helsinki",
#"streetview_entrance_url":"https://maps.google.fi/?q=http:%2F%2Fwww.hel.fi%2Fpalvelukartta%2Fkml.aspx%3Flang%3Dfi%26id%3D1&ll=60.244698,24.855857&spn=0.000005,0.006518&layer=c&cbll=60.244698,24.855857&cbp=12,185.5,,0,0&t=h&panoid=Qfr1-FBKlO3UeaczK1xpzA&z=17",
#"extra_searchwords_fi":"kesäaukiolo - avoinna läpi kesän, kuninkaankolmio",
#"accessibility_phone":"09 310 41571",
#"accessibility_email":"pk.apila@hel.fi",
#"accessibility_viewpoints":"00:unknown,11:red,12:red,13:red,21:red,22:red,23:red,31:red,32:red,33:red,41:green,51:red,52:red,61:unknown",
#"created_time":"2007-11-15T00:00:00",
#"modified_time":"2018-04-06T14:27:06"
#}


import json
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

import pprint
pp = pprint.PrettyPrinter(indent=2)

def loadjson(filename):
    with open(filename) as f:
        return json.loads(f.read())

poiUnits        = loadjson("poi-snapshot-2018-08-08.json")
ontologyWords   = loadjson("poi-ontology-word-2018-08-14.json")
ontologyTree    = {data["id"] : data for data in loadjson("poi-ontology-tree-2018-08-14.json")}
factualTaxonomy = loadjson("places/categories/factual_taxonomy.json")
# use ontology words and tree to find matching terms with ./places/categories
# licence CC 4.0 BY? Helsingin kaupungin toimipisterekisteri https://palvelukartta.hel.fi

def prepareTLabel(data):
    if len(data["parents"]) >= 1:
        return prepareTLabel(factualTaxonomy[data["parents"][0]]) + " " + data["labels"]["en"]
    else:
        return ""

def prepareWith(f, d): return {f(data) : identifier for identifier, data in d.items()}


targetCategories = prepareWith(prepareTLabel, factualTaxonomy)

def prepareSLabel(data):
    if "parent_id" in data:
        return prepareSLabel(ontologyTree[data["parent_id"]]) + " " + data["name_en"]
    else:
        return ""

sourceCategories = prepareWith(prepareSLabel, ontologyTree)

#pp.pprint(sourceCategories)

choices = list(targetCategories.keys())

# Not good enough
result = {sourceId : process.extract(sourceId, choices, limit=3, scorer=fuzz.token_set_ratio) for sourceId in sourceCategories.keys()}

# Strategy
# 1. Search full phrase from DBpedia and words one by one from DBpedia
# 2. if not found: use DBpedia lookup for full phrase and words
# 3. find similarities and fallback to word similarity


