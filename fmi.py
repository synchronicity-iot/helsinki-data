#!/usr/bin/python3
# Finnish Meteorological institute data fetcher
# eventually supports air quality, weather observations and forecasts



#fmi_apikey = "********-****-****-****-************"
from private import fmi_apikey
from integration.utils import get,xfind,xfindi,xpath,xattr,utcFromTimestamp,ngsiAppend
from lxml import etree
import re, math
from datetime import datetime, timezone, timedelta
from argparse import Namespace

# namespaces that are parsed from the response
#ns = {
#    "wfs":"http://www.opengis.net/wfs/2.0",
#    "xsi":"http://www.w3.org/2001/XMLSchema-instance",
#    "xlink":"http://www.w3.org/1999/xlink",
#    "om":"http://www.opengis.net/om/2.0",
#    "ompr":"http://inspire.ec.europa.eu/schemas/ompr/3.0",
#    "omso":"http://inspire.ec.europa.eu/schemas/omso/3.0",
#    "gml":"http://www.opengis.net/gml/3.2",
#    "gmd":"http://www.isotc211.org/2005/gmd",
#    "gco":"http://www.isotc211.org/2005/gco",
#    "swe":"http://www.opengis.net/swe/2.0",
#    "gmlcov":"http://www.opengis.net/gmlcov/1.0",
#    "sam":"http://www.opengis.net/sampling/2.0",
#    "sams":"http://www.opengis.net/samplingSpatial/2.0",
#}



wfsUrl='https://data.fmi.fi/fmi-apikey/{}/wfs'.format(fmi_apikey)
ngsiEndpoint = "https://synchronicity.cs.hut.fi/orion"

# NOTE: Not wery useful dependency for this
#from owslib.wfs import WebFeatureService
#
#w = WebFeatureService(url=wfsUrl, version='2.0.0')
#
#queries = {storedquery.id: storedquery for storedquery in w.storedqueries}

helsinkiBBox = '24.5,60,25.4,60.5'
airObservations = 'urban::observations::airquality::hourly::multipointcoverage'
weatherObservations = 'fmi::observations::weather::multipointcoverage'

# Timerange for one hour
timeformat = '%Y-%m-%dT%H:%M:%SZ'
endtime = datetime.now(timezone.utc)
starttime = endtime - timedelta(hours=1)
endtime = endtime.strftime(timeformat)
starttime = starttime.strftime(timeformat)

def wfsQuery(storedQuery):
    return get(wfsUrl, {
        'request':'getFeature',
        'storedquery_id':storedQuery,
        'bbox':helsinkiBBox,
        'starttime':starttime,
        'endtime':endtime
        })

def parseMultipointcoverage(response):
    root = etree.XML(response.encode("utf-8"))


    def parseCoords(x): return tuple(x.text.strip().split(' '))
    def parsePointId(x): return xattr(x, 'gml', 'id')[6:] # remove "point-"
    namesXml = xpath(root, '//sams:SF_SpatialSamplingFeature/sams:shape/gml:MultiPoint/gml:pointMember/gml:Point')

    posToId = {}
    stations = {}
    for n in namesXml:
        id = parsePointId(n)
        pos = parseCoords(xfind(n, 'gml:pos'))
        posToId[pos] = id
        name = xfind(n, 'gml:name').text

        stations[id] = { 'fmisid': id
                       , 'name' : name
                       , 'pos' : list(map(float, reversed(pos)))
                       }

    resultXml = xpath(root, '//om:result/gmlcov:MultiPointCoverage')[0]

    positionsTimesXml = xpath(resultXml, '//gml:domainSet/gmlcov:SimpleMultiPoint/gmlcov:positions')[0]
    positionsTimes = re.findall(r"^\s*([\d.]+)\s+([\d.]+)\s+(\d+)", positionsTimesXml.text, re.M)
    idOrder = []
    for pt in positionsTimes:
        id = posToId[(pt[0], pt[1])]
        stations[id]['timestamp'] = pt[2]
        idOrder.append(id)

    def parseNanNumber(s):
        f = float(s)
        if (math.isnan(f)): return None
        else: return f

    dataXml = xpath(resultXml, '//gml:rangeSet/gml:DataBlock/gml:doubleOrNilReasonTupleList')[0]
    data = list(map(lambda line:
            list(map(parseNanNumber, line.strip().split(' '))),
            dataXml.text.strip().splitlines()
            ))

    dataLabelsXml = xpath(resultXml, '//gmlcov:rangeType/swe:DataRecord')[0]
    def parseName(f): return f.attrib['name'].replace('_PT1H_avg', '')
    dataLabels = [parseName(field) for field in xfindi(dataLabelsXml, 'swe:field')]

    for stationId, datarow in zip(idOrder, data):
        for label, datapoint in zip(dataLabels, datarow):
            stations[stationId][label] = datapoint

    return stations




# Data ready, create ngsi

def fmiNgsiTemplate(s, ngsiType):
    r = Namespace(**s)
    ret = {
            "id": r.fmisid,
            "type": ngsiType,
            "name": r.name,
            "location": {
                "coordinates": r.pos,
                "type": "Point"
                },
            "dateObserved": utcFromTimestamp(r.timestamp),
            "source": "https://fmi.fi",
        }
    return (r, ret)

#def smartSymbol(wawa,temperature, cloudiness): # https://github.com/fmidev/smartmet-engine-observation/blob/fc0a4338750d53094b0dcc35015cb0e27f9829b1/observation/Utils.cpp
#    wawa_group1 = [0, 4, 5, 10, 20, 21, 22, 23, 24, 25]
#    wawa_group2 = [30, 31, 32, 33, 34, 35]
#
#    cloudiness_limit1 = 0
#    cloudiness_limit2 = 1
#    cloudiness_limit25 =3
#    cloudiness_limit3 = 5
#    cloudiness_limit4 = 7
#    cloudiness_limit5 = 9
#
#    if wawa in wawa_group1:
#        if cloudiness <= cloudiness_limit1: return 1
#        elif cloudiness <= cloudiness_limit2: return 2
#        elif cloudiness <= cloudiness_limit25: return 3
#        elif cloudiness <= cloudiness_limit3: return 4
#        elif cloudiness <= cloudiness_limit4: return 6
#        elif cloudiness <= cloudiness_limit5: return 7
#
#    elif wawa in wawa_group2:
#        if cloudiness <= cloudiness_limit1: return 1
#        elif cloudiness <= cloudiness_limit2: return 2
#        elif cloudiness <= cloudiness_limit25: return 3
#        elif cloudiness <= cloudiness_limit3: return 4
#        elif cloudiness <= cloudiness_limit4: return 6
#        elif cloudiness <= cloudiness_limit5: return 9
#
#    elif wawa == 40 or wawa == 41:
#        if temperature <= 0:
#            if cloudiness <= 5: return 51
#            elif cloudiness <= 7: return 54
#            elif cloudiness <= 9: return 57
#        else:
#            if cloudiness <= 5: return 31
#            elif cloudiness <= 7: return 34
#            elif cloudiness <= 9: return 37
#    
#    elif wawa == 42:
#        if temperature <= 0:
#            if cloudiness <= 5: return 53
#            elif cloudiness <= 7: return 56
#            elif cloudiness <= 9: return 59
#        else:
#            if cloudiness <= 5: return 33
#            elif cloudiness <= 7: return 36
#            elif cloudiness <= 9: return 39
#    
#    
#    elif wawa >= 50 and wawa <= 53:
#        if cloudiness <= 9: return 11
#    
#    elif wawa >= 54 and wawa <= 56:
#        if cloudiness <= 9: return 14
#    
#    elif wawa == 60:
#        if cloudiness <= 5: return 31
#        elif cloudiness <= 7: return 34
#        elif cloudiness <= 9: return 37
#    
#    elif wawa == 61:
#        if cloudiness <= 5: return 31
#        elif cloudiness <= 7: return 34
#        elif cloudiness <= 9: return 37
#
#    elif wawa == 62:
#        if cloudiness <= 5: return 32
#        elif cloudiness <= 7: return 35
#        elif cloudiness <= 9: return 38
#
#    elif wawa == 63:
#        if cloudiness <= 5: return 33
#        elif cloudiness <= 7: return 36
#        elif cloudiness <= 9: return 39
#
#    elif wawa >= 64 and wawa <= 66:
#        if cloudiness <= 9: return 17
#
#    elif wawa == 67:
#        if cloudiness <= 5: return 41
#        elif cloudiness <= 7: return 44
#        elif cloudiness <= 9: return 47
#
#    elif wawa == 68:
#        if cloudiness <= 5: return 42
#        elif cloudiness <= 7: return 45
#        elif cloudiness <= 9: return 48
#
#    elif wawa == 70:
#        if cloudiness <= 5: return 51
#        elif cloudiness <= 7: return 54
#        elif cloudiness <= 9: return 57
#
#    elif wawa == 71:
#        if cloudiness <= 5: return 51
#        elif cloudiness <= 7: return 54
#        elif cloudiness <= 9: return 57
#
#    elif wawa == 72:
#        if cloudiness <= 5: return 52
#        elif cloudiness <= 7: return 55
#        elif cloudiness <= 9: return 58
#
#    elif wawa == 73:
#        if cloudiness <= 5: return 53
#        elif cloudiness <= 7: return 56
#        elif cloudiness <= 9: return 59
#
#    elif wawa == 74:
#        if cloudiness <= 5: return 51
#        elif cloudiness <= 7: return 54
#        elif cloudiness <= 9: return 57
#
#    elif wawa == 75:
#        if cloudiness <= 5: return 52
#        elif cloudiness <= 7: return 55
#        elif cloudiness <= 9: return 58
#
#    elif wawa == 76:
#        if cloudiness <= 5: return 53
#        elif cloudiness <= 7: return 56
#        elif cloudiness <= 9: return 59
#
#    elif wawa == 77:
#        if cloudiness <= 9: return 57
#
#    elif wawa == 78:
#        if cloudiness <= 9: return 57
#
#    elif wawa == 80:
#      if temperature <= 0:
#          if cloudiness <= 5: return 51
#          elif cloudiness <= 7: return 54
#          elif cloudiness <= 9: return 57
#
#      else:
#          if cloudiness <= 5: return 21
#          elif cloudiness <= 7: return 24
#          elif cloudiness <= 9: return 27
#
#    elif wawa >= 81 and wawa <= 84:
#        if cloudiness <= 5: return 21
#        elif cloudiness <= 7: return 24
#        elif cloudiness <= 9: return 27
#
#    elif wawa == 85:
#        if cloudiness <= 5: return 51
#        elif cloudiness <= 7: return 54
#        elif cloudiness <= 9: return 57
#
#    elif wawa == 86:
#        if cloudiness <= 5: return 52
#        elif cloudiness <= 7: return 55
#        elif cloudiness <= 9: return 58
#
#    elif wawa == 87:
#        if cloudiness <= 5: return 53
#        elif cloudiness <= 7: return 56
#        elif cloudiness <= 9: return 59
#
#    elif wawa == 89:
#        if cloudiness <= 5: return 61
#        elif cloudiness <= 7: return 64
#        elif cloudiness <= 9: return 67
#    
#    else: return 0






    


def weatherObserved(s):
    #smartSymbolToWeather = {
    #        '0': 'unknown',
    #        '1': 'sunnyDay',
    #        '2': 'slightlyCloudy',
    #        '3': 'partlyCloudy',  # Added
    #        '4': 'cloudy',
    #        '6': 'veryCloudy',
    #        '7': 'overcast',
    #        '9': 'fog',
    #        '11': 'lightRain',
    #        '14': 'lightRain', # raining blood on ice? https://cdn.fmi.fi/symbol-images/smartsymbol/v3/p/14.svg
    #        '17': '?', # raining blood triangles? https://cdn.fmi.fi/symbol-images/smartsymbol/v3/p/17.svg
    #        ''
            # unused:
            #clearNight,  , mist, highClouds, lightRainShower, drizzle, lightRain, heavyRainShower, heavyRain, sleetShower, sleet,
            #hailShower, hail, shower, lightSnow, snow, heavySnowShower, heavySnow, thunderShower, thunder  }
    

    
    r, ret = fmiNgsiTemplate(s, "WeatherObserved")

    if None != r.t2m:      ret["temperature"] = r.t2m
    if None != r.ws_10min: ret["windSpeed"] = r.ws_10min
    if None != r.wg_10min: ret["gustSpeed"] = r.wg_10min
    if None != r.wd_10min:
        windDir = float(r.wd_10min)
        ret["windDirection"] = windDir - 360 if windDir > 180 else windDir
    if None != r.rh:       ret["relativeHumidity"] = float(r.rh)/100
    if None != r.td:       ret["dewPoint"] = r.td
    if None != r.r_1h:     ret["precipitation"] = r.r_1h
    if None != r.ri_10min: ret["precipitationIntensity"] = r.ri_10min
    if None != r.snow_aws: ret["snowDepth"] = r.snow_aws
    if None != r.p_sea:    ret["atmosphericPressure"] = r.p_sea
    if None != r.vis:      ret["visibilityDistance"] = r.vis
    if None != r.n_man:    ret["cloudiness"] = float(r.n_man) / 8.0
    if None != r.wawa:     ret["wmoCode4680"] = r.wawa

    return ret

#response = wfsQuery(airObservations)
response = wfsQuery(weatherObservations)
try:
    stations = parseMultipointcoverage(response)
except:
    print('Parsing ERROR, Response:')
    print(response)

results = [weatherObserved(s) for s in stations.values()]

resp = ngsiAppend(ngsiEndpoint, results)








def airQualityObserved(s):
    aqLevel = ["zero", "good", "satisfactory", "fair", "poor", "very poor"]
    r, ret = fmiNgsiTemplate(s, "AirQualityObserved")
    # CO PM etc
    airSensors = ['SO2', 'NO', 'NO2', 'O3', 'TRSC', 'CO', 'PM10', 'PM25']
    for name in airSensors:
        if s[name]:
            ret[name] = s[name]


    detailedAttrs = {} if r.AQINDEX == None else {
            "airQualityIndex": {
                "value": int(r.AQINDEX),
                "type": "integer",
                "metadata": {
                    "referenceSpecification": {"value":
                        "https://www.hsy.fi/fi/asiantuntijalle/ilmansuojelu/ilmanlaatutiedotus/Sivut/Ilmanlaatuindeksi.aspx"}
                }
            },
            "airQualityLevel": {
                "value": aqLevel[int(r.AQINDEX)],
                "type": "string",
                "metadata": {
                    "referenceSpecification": {"value":
                        "https://www.hsy.fi/fi/asiantuntijalle/ilmansuojelu/ilmanlaatutiedotus/Sivut/Ilmanlaatuindeksi.aspx"}
                }
            },
        }
    detailedAttrs['id'] = ret['id']
    detailedAttrs['type'] = ret['type']

    return ret, detailedAttrs

response = wfsQuery(airObservations)
try:
    stations = parseMultipointcoverage(response)
except:
    print('Parsing ERROR, Response:')
    print(response)

results = [airQualityObserved(s) for s in stations.values()]
keyValues = [s[0] for s in results]
detailed = [s[1] for s in results]
resp = ngsiAppend(ngsiEndpoint, keyValues)
resp = ngsiAppend(ngsiEndpoint, detailed, options="")


