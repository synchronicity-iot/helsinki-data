#!/usr/bin/env python3

# Datalähteet:
# Liikennevirasto / road.digitraffic.fi, lisenssi CC 4.0 BY
DigitrafficSourceUrl = 'https://www.digitraffic.fi/en/road-traffic/'
DigitrafficUrl = 'https://tie.digitraffic.fi/api'
ngsiEndpoint = "https://synchronicity.cs.hut.fi/orion"

# Overpass,OpenStreetMap
from overpass import API
api = API()
OpenstreetmapUrl = 'https://www.openstreetmap.org/api/0.6'

MetadataFile = './digitrafficMetadata.json'

AverageVehicleLength = 2 # m

from time import sleep
from integration.utils import getXml, xfindi, get, ngsiAppend
from numbers import Number
from itertools import tee
from math import sqrt, degrees, radians, sin, cos, atan2
from sys import argv, exc_info
import json
from datetime import datetime, timedelta

# Utils:

def isNumber(x): return isinstance(x, Number)

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


class Vector():
    def __init__(s, x,y=None):
        if y is None:
            s.y = x[1]
            s.x = x[0]
        else:
            s.x = x
            s.y = y
    def ofClockAngle(d):       return Vector(0,1).rotated(-d)
    def __len__(s):            return 2
    def __add__(a, b):         return Vector(a.x + b.x, a.y + b.y)
    def __sub__(a, b):         return Vector(a.x - b.x, a.y - b.y)
    def __rmul__(a, b):        return a.__mul__(b)
    def __mul__(a, b):
        if isNumber(b):        return Vector(a.x * b, a.y * b)
        else:                  return a.dot(b)
    def dot(a, b):             return a.x * b.x + a.y * b.y
    def cross(a, b):           return a.x * b.y - a.y * b.x
    def __truediv__(a, b):     return Vector(a.x / b, a.y / b)
    def __neg__(s):            return Vector(-s.x, -s.y)
    def __pos__(s):            return s
    def perpendicular(s):      return s.rot90cw()
    def rot90ccw(s):           return Vector(-s.y, s.x)
    def rot90cw(s):            return Vector(s.y, -s.x)
    def lengthSqrd(s):         return s.x * s.x + s.y * s.y
    def length(s):             return sqrt(s.x * s.x + s.y * s.y)
    def normalized(s):         return s / s.length()
    def angle(s):              return degrees(atan2(s.y, s.x))
    def clockAngle(s):         return -(s.rot90cw().angle()) % 360  # Clock style angle (up=0, right=90, down=180, left=270)
    def angleBetween(a, b):    return degrees(atan2(a.cross(b), a.dot(b)))
    def __repr__(s):           return "Vector({x}, {y})".format(**s.__dict__)
    def __str__(s):            return "({x}, {y})".format(**s.__dict__)

    def rotated(self,d):
        rad = radians(d); c = cos(rad); s = sin(rad)
        return Vector(self.x*c - self.y*s, self.x*s + self.y*c)

# returns distance of P to line segment A,B
def lineSegmentDistanceToPoint(A, B, P):
    a = Vector(A)
    b = Vector(B)
    p = Vector(P)
    ap = p - a
    bp = p - b
    ab = b - a
    lineNormal = ab.perpendicular().normalized()

    distance = abs(ap * lineNormal)

    ablength = ab.length()

    # t is distance to perpendicular intersection point from A
    # t is in range 0..1 if it is in range of segment
    t = ap * ab / ablength / ablength

    # t is not in range of the line segment (past ending points
    if t < 0 or t > 1:
        return min(ap.length(), bp.length())
    else:
        return distance




# Implementation:


# longitude (~x, smaller), latitude (~y, larger), distance meters
def getRoads(loc, distance):
    lon,lat=loc[0],loc[1]
    # find roads around `distance` that are marked as primary, secondary, tertiary, residential or trunk, and have a name (to filter out small random roads)
        #response = api.Get('way["highway"](around:{0},{1},{2});'.format(distance,loc[0],loc[1]))
        #request = 'way[highway~"^(primary|secondary|tertiary|residential|trunk|motorway)$"][name](around:{0},{1},{2});'.format(
    #request = 'way[highway~"^(primary.*|secondary.*|tertiary.*|residential|trunk.*|motorway.*)$"](around:{0},{1},{2});'.format(
    # no residential roads have sensors?
    sleep(0.1)
    request = 'way[highway~"^(primary|secondary|tertiary|trunk|motorway)$"](around:{0},{1},{2});'.format(
        distance, lat, lon)
    response = {}
    try:
        response = api.Get(request)
    except:
        print("ERROR: Request: " + request + "Got error: {0}".format(exc_info()))
        print("retry")
        sleep(5)
        response = api.Get(request)

    if len(response['features'])>0:
        def transformWay(way):
            way = dict(way)
            way.update(way['properties'])
            del way['properties']
            return way

        return {way['id']: transformWay(way) for way in response['features'] if way.get("properties")}
    else:
        print("WARNING: No ways for request: " + request)
        return {}

def coords(n): return (float(n.attrib['lon']), float(n.attrib['lat']))

def correctRoadDirection(sensorRoadAngle, maxErrorDegrees=90):
    def roadDir(parameters):
        (dist, (a,b), way) = parameters
        roadVector = Vector(b) - Vector(a)
        roadAngleDifference = abs(roadVector.angleBetween(Vector.ofClockAngle(sensorRoadAngle)))

        if 'oneway' in way:
            way['laneDirection'] = 'forward'
            if way['oneway'] == 'yes':
                return roadAngleDifference < maxErrorDegrees
            elif way['oneway'] == '-1': # reverse one-way road :D
                return roadAngleDifference > maxErrorDegrees
            elif way['oneway'] == 'no':
                return True
            else:
                print('WARNING: way has non-"yes" or "-1" value in oneway=' + way['oneway'])
                return True

        return True
    return roadDir



def findClosestSegment(ways, nodes, sensorRoadAngle, sensorLocation):

    distances = lambda: (
            (lineSegmentDistanceToPoint(coords(a), coords(b), sensorLocation), (coords(a),coords(b)), way)
            for wayId, way in ways.items()
            for a,b in pairwise(nodes[wayId])
            )

    try:
        return min(filter(correctRoadDirection(sensorRoadAngle), distances()))
    except ValueError:
        print('WARNING: direction filtered all ways: sensorLocation: https://www.openstreetmap.org/?mlat={}&mlon={} sensorRoadAngle: {}'.format(sensorLocation[1], sensorLocation[0], sensorRoadAngle))
        try:
            return min(filter(correctRoadDirection(sensorRoadAngle, 95), distances()))
        except ValueError:
            return [None, None, None]
    except TypeError as e:
        print("ways and nodes: ", ways, nodes, list(distances()))
        import pdb; pdb.set_trace()
        raise e

def getNodes(ways):
    nodes = {}
    for wayId, way in ways.items():
        if not 'highway' in way:
            print("WARNING: way is not a road: "+str(way))
            continue
        if way['highway'] == 'motorway' and not ('oneway' in way): #implicit oneway
            way['oneway'] = 'yes'

        wayUrl = '{}/way/{}/full'.format(OpenstreetmapUrl,wayId)
        way['url'] = wayUrl

        root = getXml(wayUrl)
        
        nodes[wayId] = list(xfindi(root, 'node'))

    return nodes


# returns (wayObject, wayObject) where the first is direction1 (suunta1), second is direction2 (suunta2)
def findMissingSensorMetadata(sensorLocation, sensorRoadAngle):
    sx = sensorLocation[0]
    sy = sensorLocation[1]

    ways = getRoads(sensorLocation, 150)


    nodes = getNodes(ways)

    if len(nodes) < 1:
        print('No nodes: https://www.openstreetmap.org/?mlat={}&mlon={}'.format(sy,sx))

    smallestDistance, closestSegment, way = findClosestSegment(ways,nodes,sensorRoadAngle,sensorLocation)
    if closestSegment == None or way == None:
        print('Sensor direction ({}) filters all: https://www.openstreetmap.org/?mlat={}&mlon={} allWays: {}'.format(sensorRoadAngle,sy,sx, ways))

    way['closestSegment'] = closestSegment

    # Debug
    a = closestSegment[0]
    b = closestSegment[1]
    ax = a[0]; ay = a[1]
    bx = b[0]; by = b[1]

    if smallestDistance > 0.0001:
        print("WayUrl: {} Smallest distance: {} GoogleUrl: https://www.google.com/maps/dir/{},{}/{},{}/{},{}//@ https://www.openstreetmap.org/?mlat={}&mlon={}".format(
            way['url'], smallestDistance, ay, ax, sy, sx, by, bx, sy,sx)
            )


    roadVector = (Vector(b) - Vector(a))
    roadAngleDifference = abs(roadVector.angleBetween(Vector.ofClockAngle(sensorRoadAngle)))

    if roadAngleDifference > 10:
        print("Oneway?: {} RoadAngle: {} SensorRoadAngle: {} difference: {} deg".format(
            way.get('oneway'), roadVector.clockAngle(), sensorRoadAngle, roadAngleDifference))

    ow = way.get('oneway')
    if ow == 'yes' or ow == '-1':
        angle = (sensorRoadAngle+180) % 360
        smallestDistance2, closestSegment2, way2 = findClosestSegment(ways, nodes, angle, sensorLocation)
        if closestSegment2 == None:
            print('Sensor direction ({}) filters all: https://www.openstreetmap.org/?mlat={}&mlon={} otherWay: {} all ways: {}'.format(angle,sy,sx, str(way), str(ways)))
            way2 = way.copy()
        way2['closestSegment'] = closestSegment2
        way['laneDirection'] = 'forward' 
        way2['laneDirection'] = 'forward' 
        return (way, way2)
    else:
        way2 = way.copy()
        if roadAngleDifference < 90:
            way['laneDirection'] = 'forward' 
            way2['laneDirection'] = 'backward' 
        else:
            way['laneDirection'] = 'backward' 
            way2['laneDirection'] = 'forward' 
        return (way, way2)

    
def getMetadata():
    # only a few, id ~5000, ~6000
    #get(DigitrafficUrl + "/v1/metadata/tms-sensors")
    sensorConstants = json.loads(get(DigitrafficUrl + '/v1/data/tms-sensor-constants'))
    sensorStations = json.loads(get(DigitrafficUrl + '/v1/metadata/tms-stations'))
    
    #transform to dictionary (object) with id as key
    metadata = {feature['id']: feature for feature in sensorStations['features']}
    for constants in sensorConstants['tmsStations']:
        metadata[constants['roadStationId']]['constantValues'] = constants['sensorConstantValues']

    # get missing way ids
    for sId, sensor in metadata.items():
        if not 'constantValues' in sensor:
            print('WARNING: no constants for sensorid: '+str(sId))
            sensor['constantValues'] = []
        ways = findMissingSensorMetadata(
                sensor['geometry']['coordinates'],
                float(next((x['value'] for x in sensor['constantValues'] if x['name'] == 'Tien_suunta'), 0.0)))
        metadata[sId]['openstreetmap'] = ways


    with open(MetadataFile, 'w') as f:
        f.write(json.dumps(metadata))

    return metadata

def loadMetadata():
    with open(MetadataFile, 'r') as f:
        return json.loads(f.read())


def getData():
    return json.loads(get(DigitrafficUrl + '/v1/data/tms-data'))['tmsStations']

def ngsiFormat(data, ids):
    jsonData = []
    for sensorId in ids:
        sensor = data[sensorId]
        for index, way in enumerate(sensor['openstreetmap']):
            number = str(index+1)
            sensorTimestamp = sensor['values']['OHITUKSET_5MIN_LIUKUVA_SUUNTA1']['measuredTime']
            roadId = 'Road-'+str(way['id'])
            segmentId = 'ClosestRoadSegment-'+str(sensorId)
            roadSegment = list(way['closestSegment'])

            #try:
            jsonData.extend([
                   { 'id':                 roadId
                   , 'type':               'Road'
                   , 'dataProvider':       'https://www.openstreetmap.org/'
                   , 'source':             way['url']
                   , 'name':               way.get('name') or way.get('int_ref')
                   , 'alternateName':      sensor['properties']['names'].get('en')  # what about other languages?
                   , 'roadClass':          way['highway']
                   , 'refRoadSegment':     segmentId
                   },
                   { 'id':                 segmentId
                   , 'type':               'RoadSegment'
                   , 'dataProvider':       'https://www.openstreetmap.org/'
                   , 'source':             way['url']
                   , 'refRoad':            roadId
                   , 'name':               way.get('name') or way.get('int_ref')
                   , 'alternateName':      sensor['properties']['names'].get('en')  # what about other languages?
                   , 'location':           {'type': 'LineString', 'coordinates': roadSegment}
                   , 'startPoint':         {'type': 'Point', 'coordinates': roadSegment[0]}
                   , 'endPoint':           {'type': 'Point', 'coordinates': roadSegment[1]}
                   , 'allowedVehicleType': 'anyVehicle'
                   , 'totalLaneNumber':    way.get('lanes')
                   , 'maximumAllowedSpeed':way.get('maxspeed')
                   , 'category':           ['oneway'] if way.get('oneway') in ['yes', '-1'] else []
                   },
                   { 'id':                 'TmsStation-'+str(sensorId)+'-'+str(number)
                   , 'type':               'TrafficFlowObserved'
                   , 'source':             DigitrafficUrl + '/v1/data/tms-data/'+sensorId +','+DigitrafficUrl+'/v1/metadata/tms-stations,'+DigitrafficUrl + '/v1/data/tms-sensor-constants'
                   #, 'vehicleType'         # Not available in realtime data atm
                   #, 'vehicleSubType'      # Not available in realtime data atm
                   , 'dataProvider':       DigitrafficSourceUrl
                   , 'location':           sensor['geometry']
                   #, 'address':            sensor['properties']['roadAddress'] # too much effort to make schema.org formatted
                   , 'refRoadSegment':     segmentId
                   , 'dateObservedTo':     sensorTimestamp
                   , 'dateObservedFrom':   (datetime.fromisoformat('2019-06-24T09:53:47Z'.replace("Z", "+00:00")) - timedelta(minutes=5)).isoformat().replace("+00:00", "Z")
                   , 'description':        'Moving average measurement during 5 minutes'
                   , 'intensity':          sensor['values']['OHITUKSET_5MIN_LIUKUVA_SUUNTA'+number]['sensorValue']
                   , 'averageVehicleSpeed': sensor['values']['KESKINOPEUS_5MIN_LIUKUVA_SUUNTA'+number]['sensorValue']
                   , 'congested':          'true' if float(sensor['values']['KESKINOPEUS_5MIN_LIUKUVA_SUUNTA{n}_VVAPAAS{n}'.format(n=number)]['sensorValue']) < 60.0 else 'false' # < 60% from speedlimit
                   , 'averageHeadwayTime': sensor['values'].get('averageHeadwayTime')
                   , 'averageGapDistance': sensor['values'].get('averageGapDistance')
                   , 'laneDirection':      way['laneDirection']
                   }])

    return jsonData

def processData(data):
    metadata = loadMetadata()
    ids = set()

    for sensor in data:
        id = str(sensor['id'])
        values = {val['name']: val for val in sensor['sensorValues']}

        if len(values) == 0:
            continue

        # add some calculated values
        for i in ['1','2']:
            try:
                # OHITUKSET_5MIN, unit: kpl/h, NOPEUS, unit: km/h
                ohitukset = values['OHITUKSET_5MIN_LIUKUVA_SUUNTA'+i]['sensorValue'] 
                keskinopeus = values['KESKINOPEUS_5MIN_LIUKUVA_SUUNTA'+i]['sensorValue']
                values['averageHeadwayTime'+i] = 60*60 / ohitukset if ohitukset > 0 else float('inf')
                values['averageGapDistance'+i] = keskinopeus * 1000.0 / ohitukset - AverageVehicleLength if ohitukset > 0 else float('inf')

                ids.add(id)
                
            except KeyError as e:
                print("WARING: missing data for id: " + id + ": "+str(e))
                ids -= set(id)
                break


        metadata[id]['values'] = values

    ngsi = ngsiFormat(metadata, ids)
    ngsiAppend(ngsiEndpoint, ngsi)





if __name__ == "__main__":

    fetchMetadata = True
    fetchData = True

    if argv[1] == 'metadata':
        fetchData = False
    elif argv[1] == 'data':
        fetchMetadata = False

    if fetchMetadata:
        getMetadata()

    if fetchData:
        processData(getData())


# id 23010
#sensorLocation = [ 24.807454151121764, 60.18978308874683, 0.0 ]
#sensorRoadAngle = 10
#id 23126
#sensorLocation = [ 24.841389915515666, 60.230059399902856, 0.0 ]
#sensorLocation = [ 24.90154942668373, 60.21196483730028, 0.0 ]

    

# request example: traffic.getRoads([ 24.8074541511217, 60.189783088746, 0.0 ], 5)
# response example:
#{'type': 'FeatureCollection',
#        'features': [....
#
#{'geometry': null,
# 'id': 287549952,
# 'properties': {
#  'alt_name:fi': 'Hagalundintie',
#  'alt_name:sv': 'Hagalundvägen',
#  'bicycle': 'no',
#  'foot': 'no',
#  'guideposted_leads_to_ref': '1',
#  'highway': 'trunk',
#  'lanes': '3',
#  'lanes:psv': '1',
#  'light:method': 'high_pressure_sodium',
#  'lit': 'yes',
#  'maxspeed': '70',
#  'name': 'Kehä I',
#  'name:fi': 'Kehä I',
#  'name:sv': 'Ring I',
#  'oneway': 'yes',
#  'placement': 'right_of:1',
#  'priority_road': 'designated',
#  'ref': '101',
#  'snowplowing': 'yes',
#  'surface': 'asphalt',
#  'transit:lanes': 'new_on_left|continue|continue'},
# 'type': 'Feature'}
#

# GET https://www.openstreetmap.org/api/0.6/way/287549952/full
#<osm version="0.6" generator="CGImap 0.7.5 (3729 thorn-01.openstreetmap.org)" copyright="OpenStreetMap and contributors" attribution="http://www.openstreetmap.org/copyright" license="http://opendatacommons.org/licenses/odbl/1-0/">
#<node id="25090043" visible="true" version="4" changeset="374493" timestamp="2008-10-16T12:20:34Z" user="Liimes" uid="3855" lat="60.1864824" lon="24.8094872"/>
#<node id="25090064" visible="true" version="5" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1873305" lon="24.8086244"/>
#<node id="25090066" visible="true" version="5" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1883552" lon="24.8079058"/>
#<node id="25090068" visible="true" version="7" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1898008" lon="24.8074481"/>
#<node id="25090071" visible="true" version="6" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1924074" lon="24.8068241"/>
#<node id="25090072" visible="true" version="6" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1932765" lon="24.8066975"/>
#<node id="25090075" visible="true" version="6" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1952284" lon="24.8067143"/>
#<node id="25090077" visible="true" version="6" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1972394" lon="24.8067921"/>
#<node id="25090079" visible="true" version="6" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1983305" lon="24.8072345"/>
#<node id="25090081" visible="true" version="9" changeset="60933304" timestamp="2018-07-21T15:27:27Z" user="Daeron" uid="38239" lat="60.1988946" lon="24.8077227">
#<tag k="lane:starts" v="left"/>
#</node>
#<node id="344905962" visible="true" version="3" changeset="60933304" timestamp="2018-07-21T15:27:28Z" user="Daeron" uid="38239" lat="60.1878273" lon="24.8082292"/>
#<node id="1126334841" visible="true" version="3" changeset="60933304" timestamp="2018-07-21T15:27:29Z" user="Daeron" uid="38239" lat="60.1893298" lon="24.8075671"/>
#<node id="1539648699" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1977732" lon="24.8069416"/>
#<node id="1703314432" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1880874" lon="24.8080629"/>
#<node id="1703314435" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1886267" lon="24.8077807"/>
#<node id="1703314442" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1889013" lon="24.8076830"/>
#<node id="1703503927" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1927449" lon="24.8067570"/>
#<node id="1703504028" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1969831" lon="24.8067646"/>
#<node id="1703504036" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1980454" lon="24.8070606"/>
#<node id="1703504058" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:30Z" user="Daeron" uid="38239" lat="60.1986277" lon="24.8074603"/>
#<node id="3608623846" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:32Z" user="Daeron" uid="38239" lat="60.1960701" lon="24.8067387"/>
#<node id="5599435096" visible="true" version="2" changeset="60933304" timestamp="2018-07-21T15:27:33Z" user="Daeron" uid="38239" lat="60.1875831" lon="24.8084138"/>
#<node id="5618360946" visible="true" version="3" changeset="67896468" timestamp="2019-03-07T19:11:53Z" user="pekkarr" uid="9417583" lat="60.1870544" lon="24.8088929">
#<tag k="bus" v="yes"/>
#<tag k="name" v="Tietäjä"/>
#<tag k="public_transport" v="stop_position"/>
#</node>
#<node id="5618360948" visible="true" version="3" changeset="67896468" timestamp="2019-03-07T19:11:53Z" user="pekkarr" uid="9417583" lat="60.1965031" lon="24.8067478">
#<tag k="bus" v="yes"/>
#<tag k="name" v="Laajaranta"/>
#<tag k="public_transport" v="stop_position"/>
#</node>
#<node id="5775203632" visible="true" version="1" changeset="60933304" timestamp="2018-07-21T15:27:22Z" user="Daeron" uid="38239" lat="60.1974676" lon="24.8068394"/>
#<node id="5775203633" visible="true" version="1" changeset="60933304" timestamp="2018-07-21T15:27:22Z" user="Daeron" uid="38239" lat="60.1943897" lon="24.8066914"/>
#<node id="5775203636" visible="true" version="1" changeset="60933304" timestamp="2018-07-21T15:27:22Z" user="Daeron" uid="38239" lat="60.1936617" lon="24.8066792"/>
#<node id="5775203637" visible="true" version="1" changeset="60933304" timestamp="2018-07-21T15:27:22Z" user="Daeron" uid="38239" lat="60.1930202" lon="24.8067173"/>
#<node id="5775203639" visible="true" version="1" changeset="60933304" timestamp="2018-07-21T15:27:22Z" user="Daeron" uid="38239" lat="60.1919577" lon="24.8069279"/>
#<node id="5775203642" visible="true" version="1" changeset="60933304" timestamp="2018-07-21T15:27:22Z" user="Daeron" uid="38239" lat="60.1909558" lon="24.8071735"/>
#<way id="287549952" visible="true" version="8" changeset="60933304" timestamp="2018-07-21T15:27:48Z" user="Daeron" uid="38239">
#<nd ref="25090043"/>
#<nd ref="5618360946"/>
#<nd ref="25090064"/>
#<nd ref="5599435096"/>
#<nd ref="344905962"/>
#<nd ref="1703314432"/>
#<nd ref="25090066"/>
#<nd ref="1703314435"/>
#<nd ref="1703314442"/>
#<nd ref="1126334841"/>
#<nd ref="25090068"/>
#<nd ref="5775203642"/>
#<nd ref="5775203639"/>
#<nd ref="25090071"/>
#<nd ref="1703503927"/>
#<nd ref="5775203637"/>
#<nd ref="25090072"/>
#<nd ref="5775203636"/>
#<nd ref="5775203633"/>
#<nd ref="25090075"/>
#<nd ref="3608623846"/>
#<nd ref="5618360948"/>
#<nd ref="1703504028"/>
#<nd ref="25090077"/>
#<nd ref="5775203632"/>
#<nd ref="1539648699"/>
#<nd ref="1703504036"/>
#<nd ref="25090079"/>
#<nd ref="1703504058"/>
#<nd ref="25090081"/>
#<tag k="alt_name:fi" v="Hagalundintie"/>
#<tag k="alt_name:sv" v="Hagalundvägen"/>
#<tag k="bicycle" v="no"/>
#<tag k="foot" v="no"/>
#<tag k="guideposted_leads_to_ref" v="1"/>
#<tag k="highway" v="trunk"/>
#<tag k="lanes" v="3"/>
#<tag k="lanes:psv" v="1"/>
#<tag k="light:method" v="high_pressure_sodium"/>
#<tag k="lit" v="yes"/>
#<tag k="maxspeed" v="70"/>
#<tag k="name" v="Kehä I"/>
#<tag k="name:fi" v="Kehä I"/>
#<tag k="name:sv" v="Ring I"/>
#<tag k="oneway" v="yes"/>
#<tag k="placement" v="right_of:1"/>
#<tag k="priority_road" v="designated"/>
#<tag k="ref" v="101"/>
#<tag k="snowplowing" v="yes"/>
#<tag k="surface" v="asphalt"/>
#<tag k="transit:lanes" v="new_on_left|continue|continue"/>
#</way>
#</osm>
