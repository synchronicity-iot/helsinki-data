
# Author: Tuomas Kinnunen (@aalto.fi)
import json
from datetime import datetime, timezone
from urllib3.exceptions import HTTPError
from urllib3 import Retry, PoolManager
from lxml import etree
import certifi


# time
def utcFromTimestamp(s):
    return datetime.fromtimestamp(int(s), timezone.utc).isoformat()

# json
def encodeJson(data): return json.dumps(data).encode("utf-8")

def decodeJson(data): return json.loads(data.decode("utf-8"))

# xml
def xpath(root, query): return root.xpath(query, namespaces=root.nsmap)
def xfindi(root, query): return root.iterfind(query, namespaces=root.nsmap)
def xfind(root, query): return root.find(query, namespaces=root.nsmap)
def xattr(root, ns, query): return root.attrib['{{{0}}}{1}'.format(root.nsmap[ns], query)]


# http
retries = Retry(total=4,backoff_factor=12, method_whitelist=False)
http = PoolManager(
        retries=retries,
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where()
        )

jsonHeaders = {"Content-Type":"application/json"}
xmlHeaders  = {"Content-Type":"application/xml"}


# query: str
# retry: retries for decode
def postJson(url, data, jsonReturn=True, retry=1):
    body = encodeJson(data)
    try:
        response = http.request("POST", url, body=body, headers=jsonHeaders)
        if jsonReturn:
            return decodeJson(response.data)
        else:
            return response.data.decode("utf-8")
    except HTTPError as e:
        print("[ERROR] HTTP response:")
        print(e.read().decode("utf-8"))
        raise e
        exit(1)
    except json.decoder.JSONDecodeError:
        if retry > 0:
            postJson(url,data,jsonReturn,retry-1)
        else:
            print("[ERROR] JSON parse:")
            print(response.data)

def get(url, data={}, retry=1):
    try:
        response = http.request("GET", url, fields=data)
        return response.data.decode("utf-8")
    except HTTPError as e:
        print("[ERROR] HTTP response: ")
        print(e.read().decode("utf-8"))
        raise e
        exit(1)

def getXml(url, data={}):
    try:
        response = http.request("GET", url, fields=data)
        return etree.XML(response.data)
    except HTTPError as e:
        print("[ERROR] HTTP response: ")
        print(e.read().decode("utf-8"))
        raise e
        exit(1)

# data: json
def ngsiAppend(url, data, options="keyValues"):
    values = {
        "actionType": "APPEND",
        "entities": data
    }
    q = "?options={}".format(options) if len(options) > 0 else ""
    resp = postJson(url + "/v2/op/update" + q, values, jsonReturn=False)
    if resp != '':
        print("NGSI Error?:")
        print(resp)
        print("Request: /v2/op/update"+q)
        print(values)

    return resp
