
Description
===========

This mostly Work-In-Progress. 

This contains the following systems

* NGSI wrapper scripts: bikerental.py, fmi.py and some incomplete scripts
* **Orion-to-Orion** subscription link: I didn't find anything similar from the
  web so I made custom cgi script, `notify`, for accepting ngsi subscription notifications
  and pushing them to local orion. There is also `relay-orion.py` for crontab
  approach, which was used before subscriptions were enabled.
* **Nginx** is used to serve access to all required systems under the same
  virtualhost (because I didn't have time to look at SNI or wildcard
  certificates and already had a normal cert.)
* Fiware
  - **Orion**
  - **Keyrock**
  - **PEP Proxy for Orion**
* Some NGSI wrappers for Helsinki open data


The following are included but not used atm.

* **Nodered** with fiware plugins and automatic admin password configuration 


Setup
=====

1. `install.sh` : docker, docker-compose, nginx, fcgiwrap
2. copy nginx configuration `synchronicity.cs.aalto.fi`
3. modify nginx configuration:
  - hostnames
  - https certificate location
  - generate dhparam
4. generate an encryption key for keyrock idm in `docker-compose.yml`
5. `run.sh` : run daemons and dockers
6. open keyrock in web browser (root of the http server by default)
6. keyrock: login as `admin@test.com`, password: `1234`
7. keyrock: change the admin password
8. keyrock: create an application to represent the Fiware Orion context broker
9. keyrock: add a PEP Proxy, enable Tokens and copy application ID, PEP Proxy user&password and token secret to `docker-compose.yml`
10. restart the `pep` docker container
11. done.

The default configuration has open read access to orion. In order to have apps that have write access, do:

1. keyrock: create app in keyrock, copy the app id
2. keyrock: add the newly created app to orion app in the list of trusted apps
3. in `docker-compose.yml`, add the app id to `PEP_TRUSTED_APPS` list in the `pep` docker and restart it
4. in your app add the Client ID and Client Secret, got from keyrock
5. auth url is `/oauth2/authorize` and token url is `/oauth2/token`, and make sure your Oauth2 client library adds the parameter `scope=jwt` or `scope=permanent` to the auth request
6. make sure that Callback URL and URL settings are correct in keyrock for the new app
7. test.

NGSI wrappers / data fetcher scripts:

1. see the top of each script to check the setting constants
2. setup crontab with intervals that you feel confortable with, e.g.
```
*/10 * * * * sleep 18s && ./synchronicity/bikerental.py
6 * * * * sleep 18s && ./synchronicity/fmi.py
```
3. the `notify` cgi script can be used as an ngsi subscription receiver, which writes the received data to the local orion. It is available via url `/notify`


Data Setup
----------

1. modify and run ./subscription-fvh.sh to get data from Forum Virium Helsinki

TODO
----

* Use jwilder's docker nginx?
* docker-compose systemd service?
* other fiware stuff: SHT-Comet?

