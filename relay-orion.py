#!/usr/bin/python

from urllib2 import Request, urlopen



request = Request('https://ngsi.fvh.fi/v2/entities')
response = urlopen(request)
entities = response.read()
if response.getcode() != 200: print("Got: " + entities)

values = """
  { "actionType": "APPEND",
    "entities": """ + entities + """
  }
"""

headers = { 'Content-Type': 'application/json' }
request = Request('https://synchronicity.cs.hut.fi/orion/v2/op/update', data=values, headers=headers)
response = urlopen(request)
if response.getcode() - 200 > 10: print("Update result: " + str(response.getcode()) + response.read())

