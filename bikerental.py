#!/usr/bin/python3
# Author: Tuomas Kinnunen (@aalto.fi)
from datetime import datetime, timezone
from time import sleep
from argparse import Namespace

from integration.utils import postJson, ngsiAppend

#import pprint
#pp = pprint.PrettyPrinter(indent=4)

graphqlEndpoint = "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql"
#ngsiEndpoint = "https://synchronicity.cs.hut.fi/orion"
ngsiEndpoint = "https://synchronicity.cs.hut.fi/orion"
license = {
        "License": "CC 4.0 BY",
        "Copyright": "HSL " + str(datetime.now().year),
        "SourceUrl": "https://api.digitransit.fi/"
} # TODO


def identity(x): return x

# query: str
#def graphql(query):
#    return postJson(graphqlEndpoint, {"query":query})


# query: str
# resultExtractor: function to extract a result that will be compared to None
# retry: number of retries in case result extraction fails (http request has separate retry system)
def graphql(query, resultExtractor=identity, retry=2):
    response = postJson(graphqlEndpoint, {"query":query})
    if response != None:
        try:
            response = resultExtractor(response) # retry
        except:
            response = None
    if response == None:
        if retry > 0:
            return graphql(query,resultExtractor,retry-1)
        else:
            print("[ERROR] Data not found in qraphql response:")
            print(response)
    return response


stationIdQuery = """{ 
bikeRentalStations {
    name 
    stationId 
}
}"""


def stationQuery(id):
    return '''{
    bikeRentalStation(id:"''' + id + '''") {
        stationId
        name
        bikesAvailable
        spacesAvailable
        lat
        lon
        allowDropoff
      }
    }
    '''

def ngsiFormat(result):
    try:
        r = Namespace(**result, 
                time = datetime.now(timezone.utc).isoformat()
                )
    except TypeError:
        print("ngsiFormat error:")
        print(result)
        print(r)
        exit(1)
    r.name = r.name.replace("(","").replace(")","") # NGSI forbidden chars: Keilaniemi (M) 

    return {
        "id": r.stationId, # TODO: change to server unique
        "type": "BikeHireDockingStation",
        "name": r.name,
        "location": {
          "coordinates": [r.lat, r.lon],
          "type": "Point"
        },
        "availableBikeNumber": r.bikesAvailable,
        "freeSlotNumber": r.spacesAvailable,
        "dateModified": r.time
    }



if __name__ == "__main__":
    stationResponse = graphql(stationIdQuery)    

    stations = stationResponse["data"]["bikeRentalStations"]

    ngsidata = []

    for station in stations:
        id = station["stationId"]

        result = graphql(stationQuery(id), lambda x: x["data"]["bikeRentalStation"])

        ngsi = ngsiFormat(result)

        #ngsiWrite([ngsi])
        ngsidata.append(ngsi)
        

        sleep(0.1)

    ngsiAppend(ngsiEndpoint, ngsidata)

