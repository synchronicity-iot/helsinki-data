curl --include \
     --user subs:03c068c5cb80a4313818982c65edeb88 \
     --request POST \
     --header "Content-Type: application/json" \
     --data-binary "{
  \"description\": \"Aalto subscription: sychronize data to synchronicity.cs.aalto.fi/orion/\",
  \"subject\": {
    \"entities\": [
      {
        \"idPattern\": \".*\"
      }
    ]
  },
  \"notification\": {
    \"http\": {
      \"url\": \"http://synchronicity.cs.aalto.fi/notify\"
    }
  },            
  \"expires\": \"2038-05-27T07:00:00.00Z\"
}" \
'https://ngsi.fvh.fi/v2/subscriptions'
