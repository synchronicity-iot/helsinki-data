#!/usr/bin/bash
set -e -x

sudo apt-get install docker docker-compose nginx fcgiwrap

# configuration
sudo mkdir -p /var/www/synchronicity/
sudo cp notify /var/www/synchronicity/
sudo cp nginx/* /etc/nginx/

sudo systemctl enable docker nginx fcgiwrap

