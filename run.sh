#!/usr/bin/bash
set -e -x

sudo systemctl start docker nginx fcgiwrap
sudo docker-compose up -d

